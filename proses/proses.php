<?php 

function koneksi()
{
	return mysqli_connect("localhost", "root", "", "perpustakaan");
}

function login()
{
	$conn = koneksi();
	$username = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);
	$query = "SELECT * FROM akun WHERE username = '$username' AND password = '$password'";
	$result = $conn->query($query);
	$cek = $result->num_rows;
	if ($cek === 1)
	{
		$ambil = $result->fetch_assoc();
		$_SESSION['login'] = $ambil;
		if ($_SESSION['login']['role'] == 'users')
		{
			echo "<script>alert('Anda berhasil login sebagai users')</script>";
			echo "<script>location='index.php'</script>";
			
		}else {
			echo "<script>alert('Anda Berhasil login sebagai admin')</script>";
			echo "<script>location='index.php'</script>";
		}
	}
	else
	{
		echo "<script>alert('Anda Gagal Login Periksa Akun Anda')</script>";
		echo "<script>location='login.php'</script>";
	}
}

function tampildatabuku()
{
	$conn = koneksi();
	$query ="SELECT * FROM buku_saya INNER JOIN produk ON buku_saya.id_produk=produk.id_produk";
	$result = $conn->query($query);
	$data = [];
	while ($row = $result->fetch_assoc()) {
		$data[] = $row;
	}
	return $data;
}

function tampilbukusaya($query)
{
	$conn = koneksi();
	$data = $query;
	$result = $conn->query($data);
	$data1 = [];
	while($row = $result->fetch_assoc()) {
		$data1[] = $row;
	}
	return $data1;
}
function ambilproduk($query)
{
	$conn = koneksi();
	$query = $query;
	$result = $conn->query($query);
	$data = [];
	while($row = $result->fetch_assoc())
	{
		$data[] = $row;
	}

	return $data;
}

function tampiluser($query)
{
	$conn = koneksi();
	$query = $query;
	$result = $conn->query($query);
	$data = [];
	while ($row = $result->fetch_assoc()) {
		$data[] = $row;
	}
	return $data;
}

function editUsers()
{
	$conn = koneksi();
	$nama = htmlspecialchars($_POST['nama']);
	$username = htmlspecialchars($_POST['username']);
	$role = htmlspecialchars($_POST['role']);
	$id = $_GET['id'];
	$query = "UPDATE akun SET nama = '$nama', username='$username', role='$role' WHERE id = '$id'";
	$result_pecah = $conn->query($query);
	if ($result_pecah === TRUE)
	{
		echo "<script>alert('anda berhasil mengedit data')</script>";
		echo "<script>location='index.php?halaman=data_users'</script>";
	}
	else 
	{
		echo "<script>alert('anda gagal mengedit data')</script>";
		echo "<script>location='index.php?halaman=data_users'</script>";
	}
}

function tampilproduk()
{
	$conn = koneksi();
	$sql = "SELECT * FROM produk";
	$result = $conn->query($sql);
	$data = [];
	while($row = $result->fetch_assoc())
	{
		$data[] = $row;
	}
	return $data;
}

function tambahdataproduk()
{
	$namaproduk = htmlspecialchars($_POST['nama_produk']);
	$hargaproduk = htmlspecialchars($_POST['harga_produk']);
	$deskripsi = htmlspecialchars($_POST['deskripsi']);
	$conn = koneksi();
	$sql = "INSERT INTO produk (nama_produk,harga_produk,deskripsi)VALUES('$namaproduk',
			 '$hargaproduk', '$deskripsi')
	";
	$result = $conn->query($sql);
	$cek = $result->num_rows;
	if ($cek == 1)
	{
		echo "<script>alert('anda gagal menambahkan data')</script>";
		echo "<script>location='index.php?halaman=produk'</script>";
	}
	else
	{
		echo "<script>alert('anda berhasil menambahkan data')</script>";
		echo "<script>location='index.php?halaman=produk'</script>";
	}
}

function editproduk()
{
	$idproduk = $_GET['id'];
	$namaproduk = htmlspecialchars($_POST['nama_produk']);
	$hargaproduk = htmlspecialchars($_POST['harga_produk']);
	$deskripsi = htmlspecialchars($_POST['deskripsi']);
	$conn = koneksi();
	$sql = "UPDATE produk SET nama_produk = '$namaproduk', harga_produk = '$hargaproduk', deskripsi = '$deskripsi' WHERE id_produk = $idproduk";
	$result = $conn->query($sql);
	if ($result === TRUE)
	{
		echo "<script>alert('anda berhasil mengupdate data')</script>";
		echo "<script>location='index.php?halaman=produk'</script>";
	}
}

function editpengguna()
{
	$id = $_GET['id'];
	$nama = htmlspecialchars($_POST['nama']);
	$username = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);
	$query = "SELECT password,role FROM akun WHERE id = '$id'";
	$conn = koneksi();
	$result = $conn->query($query);
	$data = [];
	while($row = $result->fetch_assoc())
	{
		$data[] = $row;
	}

	$password2 =  $data[0]['password'];
	$role = $data[0]['role'];


	if (!empty($_POST['password']))
	{
		$sql = "UPDATE akun SET nama = '$nama', username = '$username', password = '$password', role = '$role' WHERE id = '$id'";
	}
	else 
	{
		$sql =  "UPDATE akun SET nama = '$nama', username = '$username', password = '$password2', role = '$role' WHERE id = '$id'";
	}

	$result_set = $conn->query($sql);
	if ($result_set === TRUE)
	{
		echo "<script>alert('Anda berhasil Edit Data')</script>";
		echo "<script>location='index.php?halaman=pengguna'</script>";
	}
	else 
	{
		echo "<script>alert('Anda gagal Edit Data')</script>";
		echo "<script>location='index.php?halaman=pengguna'</script>";
	}
}

function registrasi()
{
	$nama = htmlspecialchars($_POST['nama']);
	$username = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);
	$password_confirm = htmlspecialchars($_POST['password2']);
	$role = "users";
	$conn = koneksi();
	if ($password !== $password_confirm)
	{
		echo "<script>alert('Maaf Kata Sandi Anda Harus Sama')</script>";
	}
	else 
	{
		$sql = "INSERT INTO akun (nama,username,password,role)
		VALUES
		('$nama', '$username', '$password', '$role')
		";

		$result = $conn->query($sql);
		if ($result === TRUE)
		{
			echo "<script>alert('Anda Berhasil Membuat Akun')</script>";
			echo "<script>location='login.php'</script>";
		}
		else
		{
			echo "<script>alert('Anda gagal Membuat Akun')</script>";
			echo "<script>location='registrasi.php'</script>";
		}
	}
	
}

 ?>