var myapp = function($scope){

}


var configapp = function($stateProvider, $urlRouterProvider){
	let mystate = {

		dashboard:{
			name : 'dashboard',
			url: '/users/home.php',
			controller: 'dashboardController'
		},

		data_buku: {
			name: 'Data Buku',
			url: '/users/buku.php',
			controller: 'bukuController'
		},

		data_anggota: {
			name : "Data Anggota",
			url: '/anggota',
			controller: 'anggotaController'
		},

		data_transaksi: {
			name : 'data transaksi',
			url: '/transaksi',
			controller: 'transaksiController'
		}

	};
	$stateProvider 
				.state(mystate.dashboard)
				.state(mystate.data_buku)
				.state(mystate.data_anggota)
				.state(mystate.data_transaksi);
	$urlRouterProvider
	.otherwise('/dashboard')
}

var app = angular.module('my-app', ["ui.router"]);
.config(configapp)
app.contoller('appController', myapp);
app.contoller('dashboardController', dashboard);
app.contoller('bukuController', buku);