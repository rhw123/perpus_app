-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2021 at 12:01 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpustakaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id` int(20) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id`, `nama`, `username`, `password`, `role`) VALUES
(1, 'buk mila', 'users', 'userss', 'users'),
(2, 'bambang adi', 'admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(20) NOT NULL,
  `judul_buku` varchar(100) NOT NULL,
  `pengarang` varchar(100) NOT NULL,
  `penerbit` varchar(100) NOT NULL,
  `jumlah_buku` int(20) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id_buku`, `judul_buku`, `pengarang`, `penerbit`, `jumlah_buku`, `status`) VALUES
(1, 'kisah cinta', 'bunda cinta', 'sperator', 10, 'sudah-bayar'),
(2, 'budaya melayu', 'orang melayu', 'melayu riau', 2, 'belum-bayar'),
(3, 'sangkuriang', 'suilo', 'suilo', 3, 'belum-bayar'),
(4, 'laravel', 'pak ganteng', 'pak ganteng', 20, 'sudah-bayar'),
(5, 'bila cinta datang', 'sutisna', 'sutisna', 10, 'sudah-bayar'),
(6, 'kamen rider zero one', 'antabrantah', 'antabranta', 30, 'belum-bayar'),
(7, 'kamen rider kuuga', 'kuuga', 'kuuga', 30, 'sudah-bayar'),
(8, 'kamen rider revice', 'baru jadi', 'baru jadi', 35, 'belum-bayar');

-- --------------------------------------------------------

--
-- Table structure for table `buku_saya`
--

CREATE TABLE `buku_saya` (
  `id_buku_saya` int(11) NOT NULL,
  `id_users` varchar(200) NOT NULL,
  `jumlah` int(30) NOT NULL,
  `alamat_saya` text NOT NULL,
  `status_buku` varchar(20) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `bank` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buku_saya`
--

INSERT INTO `buku_saya` (`id_buku_saya`, `id_users`, `jumlah`, `alamat_saya`, `status_buku`, `id_produk`, `bank`) VALUES
(1, '1', 5, 'jl kurnia', 'belum-lunas', 2, 'bri'),
(3, '1', 1, '<p>jl sembilang</p>\r\n', 'sudah-lunas', 1, 'bca');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `nama_produk` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga_produk` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `deskripsi`, `harga_produk`) VALUES
(1, 'buku sarjana', 'keren banget', 400000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `buku_saya`
--
ALTER TABLE `buku_saya`
  ADD PRIMARY KEY (`id_buku_saya`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `buku_saya`
--
ALTER TABLE `buku_saya`
  MODIFY `id_buku_saya` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
