<?php 

session_start();
require_once 'proses/proses.php';

if (!isset($_SESSION['login']))
{
	header("Location: login.php");
}

 ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Free Bootstrap Admin Template : Binary Admin</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body ng-app="my-app" ng-controller="appController">
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><?php echo $_SESSION['login']['nama']; ?></a> 
            </div>
  
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <?php $cek = $_SESSION['login']['role']; ?>
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="assets/img/find_user.png" class="user-image img-responsive"/>
					</li>
					
                    <?php $cek = $_SESSION['login']['role']; ?>
                        <?php if ($cek == "admin") { ?>
                            <li>
                                <a  href="index.php"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                            </li>
                            
                            <li>
                                <a  href="index.php?halaman=buku"><i class="fa fa-dashboard fa-3x"></i> List Data Pelanggan</a>
                            </li>
                            <li>
                                <a  href="index.php?halaman=produk"><i class="fa fa-dashboard fa-3x"></i>Produk</a>
                            </li>
                            <li>
                                <a  href="index.php?halaman=pengguna"><i class="fa fa-dashboard fa-3x"></i> Data Pengguna</a>
                            </li>
                             <li>
                                <a  href="logout.php"><i class="fa fa-dashboard fa-3x"></i> Logout</a>
                            </li>     
                        <?php }else  { ?>
                            <li>
                                <a  href="index.php"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                            </li>

                            <li>
                                <a  href="index.php?halaman=produk"><i class="fa fa-dashboard fa-3x"></i> Produk</a>
                            </li>

                            <li>
                                <a  href="index.php?halaman=keranjang"><i class="fa fa-dashboard fa-3x"></i> Keranjang Saya</a>
                            </li>
                            
                        
                             <li>
                                <a  href="index.php?halaman=transaksi"><i class="fa fa-dashboard fa-3x"></i> Data Transaksi</a>
                            </li>
                             <li>
                                <a  href="logout.php"><i class="fa fa-dashboard fa-3x"></i> Logout</a>
                            </li>
      

                        <?php } ?>
                        
              
				
                      
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     

                     <ui-view></ui-view>
                     <?php 
                        if ($cek === "admin")
                        {
                            if (isset($_GET['halaman'])) {
                                if ($_GET['halaman'] == 'buku')
                                {
                                    require_once 'admin/buku.php';
                                }
                                else if ($_GET['halaman'] == "produk")
                                {
                                    require_once 'admin/produk.php';
                                }
                                else if ($_GET['halaman'] == "pengguna")
                                {
                                    require_once 'admin/pengguna.php';
                                }
                                else if ($_GET['halaman'] == "hapusproduk")
                                {
                                    require_once 'admin/hapusproduk.php';
                                }
                                else if ($_GET['halaman'] == "editproduk")
                                {
                                    require_once 'admin/editproduk.php';
                                }
                                else if ($_GET['halaman'] == "editpengguna")
                                {
                                    require_once 'admin/editpengguna.php';
                                }
                                else if ($_GET['halaman'] == "hapuspengguna")
                                {
                                    require_once 'admin/hapuspengguna.php';
                                }
                                else if ($_GET['halaman'] == "hapusdatapelanggan")
                                {
                                    require_once 'admin/hapusdatapelanggan.php';
                                }
                            }else {
                                require_once 'admin/home.php';
                            }
                           
                        }
                        else 
                        {
                            if (isset($_GET['halaman']))
                        {
                            if ($_GET['halaman'] == 'buku')
                            {
                                require_once 'users/buku.php';
                            }
                            else if ($_GET['halaman'] == 'anggota')
                            {
                                require_once 'users/anggota.php';
                            }
                            else if ($_GET['halaman'] == 'transaksi')
                            {
                                require_once 'users/transaksi.php';
                            }
                            else if ($_GET['halaman'] == "produk")
                            {
                                require_once 'users/produk.php';
                            }
                            else if ($_GET['halaman'] == 'beli')
                            {
                                require_once 'users/beli.php';
                            }
                            else if ($_GET['halaman'] == 'keranjang')
                            {
                                require_once 'users/keranjang.php';
                            }
                            else if ($_GET['halaman'] == 'hapus')
                            {
                                require_once 'users/hapus.php';
                            }
                            else if ($_GET['halaman'] == 'checkout')
                            {
                                require_once 'users/checkout.php';
                            }

                            
                            
                            
                            }
                            else 
                             {
                                require_once 'home.php';
                              }
                        }
                     
                        
                     
                    


                      ?> 
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- angular -->
    <script type="text/javascript" src="assets/js/angular.js"></script>
    <script type="text/javascript" src="node_modules/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>

    <?php if ($_GET['halaman']) { ?>

    <?php if ($_GET['halaman'] == "editproduk") { ?>
        <script>
             CKEDITOR.replace( 'deskripsi' );
        </script>
    <?php } else if ($_GET['halaman'] == "produk") { ?>
        <script>
             CKEDITOR.replace( 'deskripsi' );
        </script>

    <?php }else if ($_GET['halaman'] == "checkout") { ?>

        <script>
             CKEDITOR.replace( 'alamat' );
        </script>


    <?php } ?>

    <?php } ?>
   
</body>
</html>
