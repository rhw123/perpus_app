<?php 

$ambil = ambilproduk("SELECT * FROM produk");

 ?>

<h4>halaman produk</h4>

<table class="table table-bordered"  id="dataTables-example">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Buku</th>
			<th>deskripsi</th>
			<th>harga</th>
			<th>aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; ?>
		<?php foreach ($ambil as $amb) :?>
		<tr>
			<td><?php echo $no++; ?></td>
			<td><?php echo $amb['nama_produk']; ?></td>
			<td><?php echo $amb['deskripsi']; ?></td>
			<td><?php echo number_format($amb['harga_produk']); ?></td>
			<td>
				<a href="index.php?halaman=beli&id=<?php echo $amb['id_produk']; ?>" class="btn btn-danger">Beli</a>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<script>
    CKEDITOR.replace( 'deskripsi' );
 </script>