<?php 

echo "<pre>";

print_r($_SESSION['keranjang']);

echo "</pre>";

$array = [];
if ($_SESSION['keranjang'] === $array OR empty($_SESSION['keranjang']))
{   
    echo "<script>alert('keranjang anda kosong')</script>";
    echo "<script>location='index.php?halaman=produk'</script>";
}



?>

<h3>Halaman keranjang saya</h3>

<table class = "table table-bordered table-responsive">

    <thead>
        <tr>
            <th>No</th>
            <th>Nama Buku</th>
            <th>deskripsi</th>
            <th>Jumlah Buku</th>
            <th>harga</th>
            <th>Total Harga</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $nomor = 1; ?>
        <?php foreach($_SESSION['keranjang'] as $id_produk => $jumlah) : ?>
        <?php $ambil = ambilproduk("SELECT * FROM produk WHERE id_produk = $id_produk")[0]; ?>
        <?php $totalharga = $ambil['harga_produk'] * $jumlah;  ?>
        <tr>
            <td><?php echo $nomor++ ?></td>
            <td><?php echo $ambil['nama_produk']; ?></td>
            <td><?php echo $ambil['deskripsi']; ?></td>
            <td><?php echo $jumlah; ?></td>
            <td><?php echo number_format($ambil['harga_produk']); ?></td>
            <td><?php echo number_format($totalharga); ?></td>
            <td>
                <a href="index.php?halaman=hapus&id=<?php echo $id = $id_produk ?>" class = "btn btn-danger btn-sm">Hapus</a>
            </td>
        </tr>
        <?php endforeach; ?>
      
    </tbody>
   
</table>
<a href="index.php?halaman=checkout" class = "btn btn-primary btn-sm">Chekout</a>
<a href="index.php?halaman=produk" class = "btn btn-info btn-sm">Kembali</a>