<?php 

$idproduk = $_GET['id'];
$ambil = tampiluser("SELECT * FROM produk WHERE id_produk = '$idproduk'")[0];
?>
<h3>Halaman Edit Data</h3>
<form action="" method = "POST" style="margin-top:30px">
    <div class = "form-group">
        <div class = "input-group">
            <label for="">Nama Produk</label>
            <input type="text" name = "nama_produk" class = "form-control" value = "<?php echo $ambil['nama_produk'] ?>">
        </div>
        <div class = "input-group">
            <label for="">Harga Produk</label>
            <input type="text" name = "harga_produk" class = "form-control" value = "<?php echo $ambil['harga_produk'] ?>">
        </div>
        <div class = "input-group">
            <label for="">Deskripsi Produk</label>
            <input type="text" name = "deskripsi" class = "form-control" value = "<?php echo $ambil['deskripsi'] ?>">
        </div>
        <div class = "input-group m-4" style = "margin-top:20px">
            <button type = "submit" name = "edit" class = "btn btn-primary btn-sm">Edit</button>
            <a href = "" class = "btn btn-info btn-sm" style = "margin-left:5px">Kembali</a>
        </div>
    </div>
</form>

<?php 

if (isset($_POST['edit']))
{
    editproduk();
}

?>