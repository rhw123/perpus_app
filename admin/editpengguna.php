<h3>Edit Pengguna</h3>

<?php 

$idproduk = $_GET['id'];
$ambil = tampiluser("SELECT * FROM akun WHERE id = '$idproduk'")[0];
?>
<h3>Halaman Edit Data</h3>
<form action="" method = "POST" style="margin-top:30px">
    <div class = "form-group">
        <div class = "input-group">
            <label for="">Nama Pengguna</label>
            <input type="text" name = "nama" class = "form-control" value = "<?php echo $ambil['nama'] ?>">
        </div>
        <div class = "input-group">
            <label for="">username</label>
            <input type="text" name = "username" class = "form-control" value = "<?php echo $ambil['username'] ?>">
        </div>
        <div class = "input-group">
            <label for="">Password</label>
            <input type="text" name = "password" class = "form-control">
        </div>
        <div class = "input-group m-4" style = "margin-top:20px">
            <button type = "submit" name = "edit" class = "btn btn-primary btn-sm">Edit</button>
            <a href = "" class = "btn btn-info btn-sm" style = "margin-left:5px">Kembali</a>
        </div>
    </div>
</form>

<?php 

if (isset($_POST['edit']))
{
    editpengguna();
}

?>