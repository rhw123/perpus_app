<?php 

$ambil = tampiluser("SELECT * FROM akun");

 ?>

<h3>data users</h3>

<table class="table table-bordered">
	<thead>
		<tr>
			<th>no</th>
			<th>nama</th>
			<th>username</th>
			<th>role</th>
			<th>aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; ?>
		<?php foreach($ambil as $amb) : ?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $amb['nama']; ?></td>
			<td><?php echo $amb['username']; ?></td>
			<td><?php echo $amb['role']; ?></td>
			<td>
				<a href="index.php?halaman=edit&id=<?php echo $amb['id'] ?>" class="btn btn-warning">Edit</a>
				<a href="index.php?halaman=hapus&id=<?php echo $amb['id']; ?>" class="btn btn-danger">Hapus</a>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>