<?php 

$ambil = tampildatabuku();

 ?>
<h2>Data Pembelian</h2>

<table class="table table-bordered"  id="dataTables-example">
	<thead>
		<tr>
			<th>No</th>
            <th>Nama Pembeli</th>
            <th>Alamat Pembeli</th>
            <th>Status Buku</th>
            <th>Nama Produk</th>
            <th>Bank</th>
            <th>Aksi</th>     
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; ?>
		
		<?php foreach ($ambil as $amb) : ?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $_SESSION['login']['nama']; ?></td>
			<td><?php echo $amb['alamat_saya']; ?></td>
			<td><?php echo $amb['status_buku']; ?></td>
			<td><?php echo $amb['nama_produk']; ?></td>
			<td><?php echo $amb['bank']; ?></td>
			<td>
				<a href="index.php?halaman=hapusdatapelanggan&id=<?php echo $amb['id_buku_saya'] ?>" class=" btn btn-danger">hapus</a>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>