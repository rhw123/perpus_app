<?php 

$ambil = tampiluser("SELECT * FROM akun");

?>

<h3>data pengguna</h3>

<table class = "table table-bordered" id = "dataTables-example">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Username</th>
            <th>Role</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $nomor = 1; ?>
        <?php foreach($ambil as $amb): ?>
        <tr>
            <td><?php echo $nomor++ ?></td>
            <td><?php echo $amb['nama']; ?></td>
            <td><?php echo $amb['username']; ?></td>
            <td><?php echo $amb['role']; ?></td>
            <td>
                <a href="index.php?halaman=editpengguna&id=<?php echo $amb['id'] ?>" class = "btn btn-warning btn-sm">Edit</a>
                <a href="index.php?halaman=hapuspengguna&id=<?php echo $amb['id'] ?>" class = "btn btn-danger btn-sm">Hapus</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>