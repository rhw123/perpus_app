<?php 

$ambil = tampilproduk();



?>

<h3>data produk</h3>

<table class = "table table-bordered"  id="dataTables-example">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Buku</th>
            <th>Harga Produk</th>
            <th>Deskripsi</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $nomor = 1;  ?>
        <?php foreach($ambil as $amb): ?>
        <tr>
            <td><?php echo $nomor++ ?></td>
            <td><?php echo $amb['nama_produk']; ?></td>
            <td>Rp. <?php echo number_format( $amb['harga_produk']); ?></td>
            <td><?php echo $amb['deskripsi']; ?></td>
            <td>
                <a href="index.php?halaman=editproduk&id=<?php echo $amb['id_produk']; ?>" class = "btn btn-warning btn-sm">Edit</a>
                <a href="index.php?halaman=hapusproduk&id=<?php echo $amb['id_produk']; ?>" class = "btn btn-danger btn-sm" >Hapus</a>
            </td>
        </tr>
        <?php endforeach; ?>
       
    </tbody>
</table>
<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
  Tambah Data
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Produk</h4>
      </div>
      <div class="modal-body">
       <form action="" method = "POST">
             <div class="form-group">
                <span class="input-group-addon" >Nama Produk</span>
                <input type="text" class="form-control"  name = "nama_produk" required>
            </div>
            <div class = "form-group">
                <span class = "input-group-addon">Harga Produk</span>
                <input type="text" class = "form-control" name = "harga_produk" required>
            </div>
            <div class = "form-group">
                <span class = "input-group-addon">Deskripsi Produk</span>
                <input type="text" class = "form-control" name = "deskripsi" required>
            </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" name = "tambah" class="btn btn-primary">Simpan</button>
       </form>
      </div>
      
    </div>
  </div>
</div>

<?php 

if (isset($_POST['tambah']))
{
    tambahdataproduk();
}

?>